{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Matplotlib: plotting in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This material is taken from http://www.scipy-lectures.org/intro/matplotlib/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "Matplotlib is probably the single most used Python package for 2D-graphics. It provides both a very quick way to visualize data from Python and publication-quality figures in many formats. We are going to explore matplotlib in interactive mode covering most common cases."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### IPython and the matplotlib mode\n",
    "\n",
    "IPython is an enhanced interactive Python shell that has lots of interesting features including named inputs and outputs, access to shell commands, improved debugging and many more. It is central to the scientific-computing workflow in Python for its use in combination with Matplotlib:\n",
    "For interactive matplotlib sessions with Matlab/Mathematica-like functionality, we use IPython with it’s special Matplotlib mode that enables non-blocking plotting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the IPython notebook, we insert, at the beginning of the notebook the following magic:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### pyplot\n",
    "pyplot provides a procedural interface to the matplotlib object-oriented plotting library. It is modeled closely after Matlab™. Therefore, the majority of plotting commands in pyplot have Matlab™ analogs with similar arguments. Important commands are explained with interactive examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating a simple plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matplotlib comes with a set of default settings that allow customizing all kinds of properties. You can control the defaults of almost every property in matplotlib: figure size and dpi, line width, color and style, axes, axis and grid properties, text and font properties and so on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "plt.plot(X, C)\n",
    "plt.plot(X, S)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Instantiating defaults\n",
    "In the script below, we’ve instantiated (and commented) all the figure settings that influence the appearance of the plot.\n",
    "\n",
    "The settings have been explicitly set to their default values, but now you can interactively play with the values to explore their affect (see Line properties and Line styles below)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Create a figure of size 8x6 inches, 80 dots per inch\n",
    "plt.figure(figsize=(8, 6), dpi=80)\n",
    "\n",
    "# Create a new subplot from a grid of 1x1\n",
    "plt.subplot(1, 1, 1)\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "# Plot cosine with a blue continuous line of width 1 (pixels)\n",
    "plt.plot(X, C, color=\"blue\", linewidth=1.0, linestyle=\"-\")\n",
    "\n",
    "# Plot sine with a green continuous line of width 1 (pixels)\n",
    "plt.plot(X, S, color=\"green\", linewidth=1.0, linestyle=\"-\")\n",
    "\n",
    "# Set x limits\n",
    "plt.xlim(-4.0, 4.0)\n",
    "\n",
    "# Set x ticks\n",
    "plt.xticks(np.linspace(-4, 4, 9, endpoint=True))\n",
    "\n",
    "# Set y limits\n",
    "plt.ylim(-1.0, 1.0)\n",
    "\n",
    "# Set y ticks\n",
    "plt.yticks(np.linspace(-1, 1, 5, endpoint=True))\n",
    "\n",
    "# Save figure using 72 dots per inch\n",
    "# plt.savefig(\"exercice_2.png\", dpi=72)\n",
    "\n",
    "# Show result on screen\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Changing colors and line widths\n",
    "First step, we want to have the cosine in blue and the sine in red and a slighty thicker line for both of them. We’ll also slightly alter the figure size to make it more horizontal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "plt.figure(figsize=(10, 6), dpi=80)  # <--\n",
    "plt.subplot(1, 1, 1)\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "plt.plot(X, C, color=\"blue\", linewidth=2.5, linestyle=\"-\")  # <--\n",
    "plt.plot(X, S, color=\"red\",  linewidth=2.5, linestyle=\"-\")  # <--\n",
    "\n",
    "plt.xlim(-4.0, 4.0)\n",
    "plt.xticks(np.linspace(-4, 4, 9, endpoint=True))\n",
    "plt.ylim(-1.0, 1.0)\n",
    "plt.yticks(np.linspace(-1, 1, 5, endpoint=True))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting limits\n",
    "Current limits of the figure are a bit too tight and we want to make some space in order to clearly see all data points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 6), dpi=80)\n",
    "plt.subplot(1, 1, 1)\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "plt.plot(X, C, color=\"blue\", linewidth=2.5, linestyle=\"-\")\n",
    "plt.plot(X, S, color=\"red\",  linewidth=2.5, linestyle=\"-\")\n",
    "\n",
    "plt.xlim(X.min() * 1.1, X.max() * 1.1)  # <--\n",
    "plt.xticks(np.linspace(-4, 4, 9, endpoint=True))\n",
    "plt.ylim(C.min() * 1.1, C.max() * 1.1)  # <--\n",
    "plt.yticks(np.linspace(-1, 1, 5, endpoint=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Settings ticks\n",
    "Current ticks are not ideal because they do not show the interesting values (+/-π,+/-π/2) for sine and cosine. We’ll change them such that they show only these values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 6), dpi=80)\n",
    "plt.subplot(1, 1, 1)\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "plt.plot(X, C, color=\"blue\", linewidth=2.5, linestyle=\"-\")\n",
    "plt.plot(X, S, color=\"red\",  linewidth=2.5, linestyle=\"-\")\n",
    "\n",
    "plt.xlim(X.min() * 1.1, X.max() * 1.1)\n",
    "plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi])  # <--\n",
    "plt.ylim(C.min() * 1.1, C.max() * 1.1)\n",
    "plt.yticks([-1, 0, +1])  # <--"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting tick labels\n",
    "Ticks are now properly placed but their label is not very explicit. We could guess that 3.142 is π but it would be better to make it explicit. When we set tick values, we can also provide a corresponding label in the second argument list. Note that we’ll use latex to allow for nice rendering of the label."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 6), dpi=80)\n",
    "plt.subplot(1, 1, 1)\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "plt.plot(X, C, color=\"blue\", linewidth=2.5, linestyle=\"-\")\n",
    "plt.plot(X, S, color=\"red\",  linewidth=2.5, linestyle=\"-\")\n",
    "\n",
    "plt.xlim(X.min() * 1.1, X.max() * 1.1)\n",
    "plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],\n",
    "          [r'$-\\pi$', r'$-\\pi/2$', r'$0$', r'$+\\pi/2$', r'$+\\pi$'])  # <--\n",
    "plt.ylim(C.min() * 1.1, C.max() * 1.1)\n",
    "plt.yticks([-1, 0, +1],\n",
    "          [r'$-1$', r'$0$', r'$+1$'])  # <--"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Moving spines\n",
    "Spines are the lines connecting the axis tick marks and noting the boundaries of the data area. They can be placed at arbitrary positions and until now, they were on the border of the axis. We’ll change that since we want to have them in the middle. Since there are four of them (top/bottom/left/right), we’ll discard the top and right by setting their color to none and we’ll move the bottom and left ones to coordinate 0 in data space coordinates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 6), dpi=80)\n",
    "plt.subplot(1, 1, 1)\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "plt.plot(X, C, color=\"blue\", linewidth=2.5, linestyle=\"-\")\n",
    "plt.plot(X, S, color=\"red\",  linewidth=2.5, linestyle=\"-\")\n",
    "\n",
    "plt.xlim(X.min() * 1.1, X.max() * 1.1)\n",
    "plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],\n",
    "          [r'$-\\pi$', r'$-\\pi/2$', r'$0$', r'$+\\pi/2$', r'$+\\pi$'])\n",
    "plt.ylim(C.min() * 1.1, C.max() * 1.1)\n",
    "plt.yticks([-1, 0, +1],\n",
    "          [r'$-1$', r'$0$', r'$+1$'])\n",
    "\n",
    "ax = plt.gca()  # gca stands for 'get current axis'\n",
    "ax.spines['right'].set_color('none')\n",
    "ax.spines['top'].set_color('none')\n",
    "ax.xaxis.set_ticks_position('bottom')\n",
    "ax.spines['bottom'].set_position(('data',0))\n",
    "ax.yaxis.set_ticks_position('left')\n",
    "ax.spines['left'].set_position(('data',0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adding a legend\n",
    "Let’s add a legend in the upper left corner. This only requires adding the keyword argument label (that will be used in the legend box) to the plot commands."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 6), dpi=80)\n",
    "plt.subplot(1, 1, 1)\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "plt.plot(X, C, color=\"blue\", linewidth=2.5, linestyle=\"-\",\n",
    "         label=\"cosine\")  # <--\n",
    "plt.plot(X, S, color=\"red\",  linewidth=2.5, linestyle=\"-\",\n",
    "         label=\"sine\")  # <--\n",
    "\n",
    "plt.legend(loc='upper left')  # <--\n",
    "\n",
    "plt.xlim(X.min() * 1.1, X.max() * 1.1)\n",
    "plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],\n",
    "          [r'$-\\pi$', r'$-\\pi/2$', r'$0$', r'$+\\pi/2$', r'$+\\pi$'])\n",
    "plt.ylim(C.min() * 1.1, C.max() * 1.1)\n",
    "plt.yticks([-1, 0, +1],\n",
    "          [r'$-1$', r'$0$', r'$+1$'])\n",
    "\n",
    "ax = plt.gca()  # gca stands for 'get current axis'\n",
    "ax.spines['right'].set_color('none')\n",
    "ax.spines['top'].set_color('none')\n",
    "ax.xaxis.set_ticks_position('bottom')\n",
    "ax.spines['bottom'].set_position(('data',0))\n",
    "ax.yaxis.set_ticks_position('left')\n",
    "ax.spines['left'].set_position(('data',0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Annotate some points\n",
    "Let’s annotate some interesting points using the annotate command. We chose the 2π/3 value and we want to annotate both the sine and the cosine. We’ll first draw a marker on the curve as well as a straight dotted line. Then, we’ll use the annotate command to display some text with an arrow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 6), dpi=80)\n",
    "plt.subplot(1, 1, 1)\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "plt.plot(X, C, color=\"blue\", linewidth=2.5, linestyle=\"-\",\n",
    "         label=\"cosine\")  # <--\n",
    "plt.plot(X, S, color=\"red\",  linewidth=2.5, linestyle=\"-\",\n",
    "         label=\"sine\")  # <--\n",
    "\n",
    "plt.legend(loc='upper left')  # <--\n",
    "\n",
    "plt.xlim(X.min() * 1.1, X.max() * 1.1)\n",
    "plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],\n",
    "          [r'$-\\pi$', r'$-\\pi/2$', r'$0$', r'$+\\pi/2$', r'$+\\pi$'])\n",
    "plt.ylim(C.min() * 1.1, C.max() * 1.1)\n",
    "plt.yticks([-1, 0, +1],\n",
    "          [r'$-1$', r'$0$', r'$+1$'])\n",
    "\n",
    "ax = plt.gca()  # gca stands for 'get current axis'\n",
    "ax.spines['right'].set_color('none')\n",
    "ax.spines['top'].set_color('none')\n",
    "ax.xaxis.set_ticks_position('bottom')\n",
    "ax.spines['bottom'].set_position(('data',0))\n",
    "ax.yaxis.set_ticks_position('left')\n",
    "ax.spines['left'].set_position(('data',0))\n",
    "\n",
    "t = 2 * np.pi / 3\n",
    "plt.plot([t, t], [0, np.cos(t)], color='blue', linewidth=2.5, linestyle=\"--\")\n",
    "plt.scatter([t, ], [np.cos(t), ], 50, color='blue')\n",
    "\n",
    "plt.annotate(r'$cos(\\frac{2\\pi}{3})=-\\frac{1}{2}$',\n",
    "             xy=(t, np.cos(t)), xycoords='data',\n",
    "             xytext=(-90, -50), textcoords='offset points', fontsize=16,\n",
    "             arrowprops=dict(arrowstyle=\"->\", connectionstyle=\"arc3,rad=.2\"))\n",
    "\n",
    "plt.plot([t, t],[0, np.sin(t)], color='red', linewidth=2.5, linestyle=\"--\")\n",
    "plt.scatter([t, ],[np.sin(t), ], 50, color='red')\n",
    "\n",
    "plt.annotate(r'$sin(\\frac{2\\pi}{3})=\\frac{\\sqrt{3}}{2}$',\n",
    "             xy=(t, np.sin(t)), xycoords='data',\n",
    "             xytext=(+10, +30), textcoords='offset points', fontsize=16,\n",
    "             arrowprops=dict(arrowstyle=\"->\", connectionstyle=\"arc3,rad=.2\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Devil is in the details\n",
    " The tick labels are now hardly visible because of the blue and red lines. We can make them bigger and we can also adjust their properties such that they’ll be rendered on a semi-transparent white background. This will allow us to see both the data and the labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 6), dpi=80)\n",
    "plt.subplot(1, 1, 1)\n",
    "\n",
    "X = np.linspace(-np.pi, np.pi, 256, endpoint=True)\n",
    "C, S = np.cos(X), np.sin(X)\n",
    "\n",
    "plt.plot(X, C, color=\"blue\", linewidth=2.5, linestyle=\"-\",\n",
    "         label=\"cosine\")  # <--\n",
    "plt.plot(X, S, color=\"red\",  linewidth=2.5, linestyle=\"-\",\n",
    "         label=\"sine\")  # <--\n",
    "\n",
    "plt.legend(loc='upper left')  # <--\n",
    "\n",
    "plt.xlim(X.min() * 1.1, X.max() * 1.1)\n",
    "plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],\n",
    "          [r'$-\\pi$', r'$-\\pi/2$', r'$0$', r'$+\\pi/2$', r'$+\\pi$'])\n",
    "plt.ylim(C.min() * 1.1, C.max() * 1.1)\n",
    "plt.yticks([-1, 0, +1],\n",
    "          [r'$-1$', r'$0$', r'$+1$'])\n",
    "\n",
    "ax = plt.gca()  # gca stands for 'get current axis'\n",
    "ax.spines['right'].set_color('none')\n",
    "ax.spines['top'].set_color('none')\n",
    "ax.xaxis.set_ticks_position('bottom')\n",
    "ax.spines['bottom'].set_position(('data',0))\n",
    "ax.yaxis.set_ticks_position('left')\n",
    "ax.spines['left'].set_position(('data',0))\n",
    "\n",
    "t = 2 * np.pi / 3\n",
    "plt.plot([t, t], [0, np.cos(t)], color='blue', linewidth=2.5, linestyle=\"--\")\n",
    "plt.scatter([t, ], [np.cos(t), ], 50, color='blue')\n",
    "\n",
    "plt.annotate(r'$cos(\\frac{2\\pi}{3})=-\\frac{1}{2}$',\n",
    "             xy=(t, np.cos(t)), xycoords='data',\n",
    "             xytext=(-90, -50), textcoords='offset points', fontsize=16,\n",
    "             arrowprops=dict(arrowstyle=\"->\", connectionstyle=\"arc3,rad=.2\"))\n",
    "\n",
    "plt.plot([t, t],[0, np.sin(t)], color='red', linewidth=2.5, linestyle=\"--\")\n",
    "plt.scatter([t, ],[np.sin(t), ], 50, color='red')\n",
    "\n",
    "plt.annotate(r'$sin(\\frac{2\\pi}{3})=\\frac{\\sqrt{3}}{2}$',\n",
    "             xy=(t, np.sin(t)), xycoords='data',\n",
    "             xytext=(+10, +30), textcoords='offset points', fontsize=16,\n",
    "             arrowprops=dict(arrowstyle=\"->\", connectionstyle=\"arc3,rad=.2\"))\n",
    "\n",
    "for label in ax.get_xticklabels() + ax.get_yticklabels():\n",
    "    label.set_fontsize(16)\n",
    "    label.set_bbox(dict(facecolor='white', edgecolor='None', alpha=0.65))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "Please go to http://www.scipy-lectures.org/intro/matplotlib/index.html#regular-plots, and start working on the exercises, copying the starting point code samples from the exercise to this notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
