{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PDAP 2016 - Lecture 8\n",
    "(c) 2016 Andreas.Hilboll@uni-bremen.de"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "plt.style.use('ggplot')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to deal with large datasets\n",
    "When dealing with large datasets, it can be useful to split the data into chunks before processing.  This can have several benefits:\n",
    "\n",
    "1. When processing one chunk fails, you still get a result for the other chunks.\n",
    "2. When developing your data analysis code, you can try it out on smaller chunks, leading to shorter computation time and thus quicker development."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at an example, where we work with a time-series of 1 year length, with one value every minute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "index = pd.DatetimeIndex(start='2016-01-01',\n",
    "                         end='2016-12-31',\n",
    "                         freq='1min')\n",
    "print('This index has', index.size, 'elements')\n",
    "np.random.seed(2)\n",
    "testdata = pd.Series(np.random.randn(index.size),\n",
    "                     index=index)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "testdata.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's suppose we analyze this time-series in a function `analyze_ts`, which we define first.  In this example, it only calculates the daily mean."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def analyze_ts(ts):\n",
    "    return ts.resample('D').mean()\n",
    "\n",
    "%timeit analyze_ts(testdata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we execute this analysis function for the whole time-series, it can get slow (not in our example, but in your application). Therefore, we can split the time-series into smaller, for example monthly, chunks, to speed up the `analyze_ts` function.  This way, we can work on `analyze_ts` in quicker iterations until it does what we want it to do."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "for m in np.unique(testdata.index.month):\n",
    "    monthdata = testdata[testdata.index.month == m]\n",
    "\n",
    "print('Month', m, 'has', monthdata.size, 'elements.')    \n",
    "%timeit analyze_ts(monthdata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we're done with `analyze_ts`, we can either use it directly on the whole dataset.  \n",
    "\n",
    "Or, in cases where the `analyze_ts` function is written in a way which only works correctly on the chunks individually but not the full dataset as a whole, we can use a loop to stick the chunks together again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "result = pd.Series()\n",
    "for m in np.unique(testdata.index.month):\n",
    "    monthdata = testdata[testdata.index.month == m]\n",
    "    tmpdata = analyze_ts(monthdata)\n",
    "    result = result.append(tmpdata)\n",
    "print(result.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Create a time-series for one day, with one random value for each second.  Analyze this time-series by calculating the 30-minute median.  Compare the runtimes of applying this analysis function to the whole day and to an individual 30-minute interval.  Stick together the individual chunks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to deal with missing data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Outlier detection\n",
    "Using the `testdata` from above, let's suppose that we know that data points with an absolute value $>3.55$ are outliers.  We can create a boolean mask to select only these values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "mask = testdata.abs() > 4.35\n",
    "print('There are', mask.sum(), 'data points larger than 4.35')\n",
    "mask.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can look at those data points only"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "testdata[mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, if you want to exclude these data points from further analysis, you can assign them the no-data value `np.nan`. Pandas automatically \"does the right thing\" with missing data; see http://pandas.pydata.org/pandas-docs/stable/missing_data.html for details."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "testdata[mask] = np.nan\n",
    "testdata[mask]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "testdata.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Find out what the `isnull()` and `notnull()` methods of the `testdata` do."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Try out how Pandas handles missing data in computations.  For example, look at the `.sum()`, `.mean()`, ..., of the data frame.  Compare to the `.sum()` and `.mean()` of the underlying NumPy array `testdata.values`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Filling missing data points\n",
    "Sometimes, you might want to fill missing data values.  The easiest way to do this is the `fillna()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "filldata = testdata.copy()\n",
    "print(filldata[mask])\n",
    "filldata.fillna(123.0)[mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "What's going wrong here?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "filldata = testdata.copy()\n",
    "print('before filling:')\n",
    "print(filldata[mask])\n",
    "print()\n",
    "print('while filling:')\n",
    "print(filldata.fillna(123.0)[mask])\n",
    "print()\n",
    "print('after filling:')\n",
    "print(filldata[mask])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interpolation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In other situations, you might want to infer the data values at the missing points from the surrounding data.  This is called *interpolation*.  Pandas objects have this functionality built-in, using SciPy.  Several different "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.random.seed(2)\n",
    "ser = pd.Series(np.arange(1, 10.1, .25)**2 + np.random.randn(37))\n",
    "bad = np.array([4, 13, 14, 15, 16, 17, 18, 20, 29])\n",
    "ser[bad] = np.nan\n",
    "# try different methods for interplation\n",
    "methods = ['linear', 'quadratic', 'cubic']\n",
    "df = pd.DataFrame()\n",
    "for m in methods:\n",
    "    df[m] = ser.interpolate(method=m)\n",
    "\n",
    "# or, using dictionary comprehension\n",
    "# df = pd.DataFrame({m: ser.interpolate(method=m) for m in methods})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "plt.style.use('ggplot')\n",
    "fig, ax = plt.subplots()\n",
    "ser.plot(style='.', label='original data',\n",
    "         markersize=10, ax=ax)\n",
    "df.plot(ax=ax)\n",
    "ax.legend(loc='upper left')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll come back to interpolation in a later lecture."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculations with missing data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often, when you have data with missing values, it is enough to simply remove the invalid data points using `notnull` as an index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "valid_data = ser[ser.notnull()]\n",
    "print(valid_data.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And you can always just extract the underlying NumPy array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ser.values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pandas has the basic statistical functions built in, to deal with missing data conveniently.  If you need additional functionality, you can always extract the underlying NumPy array."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, then you need to take care of the `np.nan` values manually."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ser.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ser.values.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.nanmean(ser.values)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is the `masked_array` class in NumPy to deal with these situations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "masked_ser = np.ma.masked_invalid(ser.values)\n",
    "print(type(masked_ser))\n",
    "print(masked_ser)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `mask` attribute of the `masked_array` is identical to the `isnull()` of the original Pandas object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "masked_ser.mask == ser.isnull()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And `masked_array` objects have the same built-in functions as regular NumPy arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(masked_ser.mean())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This becomes really interesting when using advanced statistics, using the `scipy.stats.mstats` module (later)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Using `np.ma.masked_where`, create a masked array from `ser` where every value of `ser` which is larger than 40 and smaller than 50 is masked.  Calculate the mean.\n",
    "\n",
    "How could you do this directly with the `pd.Series` object, without converting to `masked_array` first?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to deal with datetimes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Different types of datetimes\n",
    "There are different concepts of representing time stamps in Python:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The `datetime` module\n",
    "\n",
    "The `datetime` module is Python's standard way of dealing with timestamp objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import datetime\n",
    "# year, month, day, hour, minute, second, microsecond\n",
    "d = datetime.datetime(2016, 12, 24, 23, 45, 6, 0)\n",
    "print(d, type(d))\n",
    "\n",
    "# now\n",
    "print(datetime.datetime.now())\n",
    "\n",
    "# from a string\n",
    "print(datetime.datetime.strptime('2016-12-24 23:45:06',\n",
    "                                 '%Y-%m-%d %H:%M:%S'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A `datetime` object can be split into a `date` and a `time` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(d.date(), type(d.date()))\n",
    "print(d.time(), type(d.time()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For arithmetic with `datetime` objects, there is `datetime.timedelta`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "d + datetime.timedelta(hours=1, microseconds=321)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For conversion to strings, there are two different possiblilities:\n",
    "\n",
    "1. `datetime.datetime.strftime`\n",
    "2. `str.format`\n",
    "3. `datetime.datetime.isoformat` for creating an ISO formatted string\n",
    "\n",
    "Documentation on string formating of `datetime` objects is available at https://docs.python.org/3.5/library/datetime.html#strftime-strptime-behavior."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('using strftime:         ', d.strftime('%Y-%m-%d %H:%M'))\n",
    "print('using isoformat:        ', d.isoformat())\n",
    "print('using string formating:  {:%Y-%m-%d}'.format(d))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Play around a bit with `datetime` objects.  For example, create a `datetime` for yesterday at noon.  Also try converting strings to datetimes and vice versa."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### NumPy `datetime64` dtype\n",
    "NumPy arrays can have the data type `datetime64`.  Documentation is available at https://docs.scipy.org/doc/numpy/reference/arrays.datetime.html."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "n = np.datetime64('2016-12-24T23:45:06.000000')\n",
    "print(n, type(n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These `datetime64` elements can have different *resolution*; supported are resolutions from years to attoseconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(np.datetime64('2005-02-25'),\n",
    "      np.datetime64('2005-02-25').dtype)\n",
    "print(np.datetime64('2005-02'),\n",
    "      np.datetime64('2005-02').dtype)\n",
    "print(np.datetime64('2005-02', 'D'),\n",
    "      np.datetime64('2005-02', 'D').dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For arithmetic with `datetime64` dtypes, there is `numpy.timedelta64`, which supports different time units."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(n + np.timedelta64(10, 'D'))\n",
    "print(n + np.timedelta64(10, 'h'))\n",
    "print(n + np.timedelta64(10, 'm'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Direct options to convert a `datetime64` to a string are limited.  For more flexibility, you need to convert to `datetime` first (see below)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('using astype:         ', n.astype(str))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Play around with NumPy's `datetime64`.  Try out different resolutions, and see what happens when you do arithmetic with different resolutions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pandas `Timestamp`\n",
    "Pandas has a class `pd.Timestamp`, which can in many respects be used like `datetime` objects, just that they support element-wise operation on arrays (or, rather, Pandas objects).  Documentation is available at http://pandas.pydata.org/pandas-docs/stable/timeseries.html."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "p = pd.Timestamp('2016-12-24T23:45:06.0')\n",
    "print(p, type(p))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "p.day, p.month, p.second"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Conversion between the different types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### from `pd.Timestamp`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(p.to_datetime(), type(p.to_datetime()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(p.to_datetime64(), type(p.to_datetime64()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### from `np.datetime64`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(n.astype(datetime.datetime), type(n.astype(datetime.datetime)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# !!! CAUTION !!!\n",
    "print(n.astype(pd.Timestamp), type(n.astype(pd.Timestamp)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# !!! instead, use !!!\n",
    "print(pd.Timestamp(n), type(pd.Timestamp(n)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### from `datetime.datetime`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(np.datetime64(d), type(np.datetime64(d)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(pd.Timestamp(d), type(pd.Timestamp(d)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Play around with conversions from one data type to another.  See what happens when you convert NumPy `datetime64` with different resolutions into other data types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Arrays, lists, ..., of time stamps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dts = [datetime.datetime(2016, 12, d) for d in range(1,32)]\n",
    "print(dts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "nps = np.arange('2016-12-01', '2017-01-01', dtype='datetime64')\n",
    "print(nps, type(nps), nps.dtype)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pds = pd.DatetimeIndex(start='2016-12-01', end='2016-12-31', freq='D')\n",
    "print(pds, type(pds), pds.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The nice thing about the Pandas way of doing dates is that it's possible to access individual components element-wise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pds.day"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.datetime64(dts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.array(dts, dtype='datetime64')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Timestamp` is the data type for a single time stamp object, comparable to `datetime.datetime`.  For creating a Pandas array (or, `DatetimeIndex` from a list of dates, use `pd.DatetimeIndex`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pd.Timestamp(dts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pd.DatetimeIndex(dts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pd.DatetimeIndex(nps)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Explore the different ways to create evenly-spaced datetime arrays in NumPy and Pandas.  Play around with different time resolutions and different intervals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Other useful tools"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Converting numbers to datetime\n",
    "The NetCDF4 module in Python has very useful functions `num2date` and `date2num`.  These functions \"understand\" human-language descriptions, like *days since 2016-01-01*, and allow for easy, array-wise conversion of numbers to datetime objects and vice versa.  Documentation is available at https://unidata.github.io/netcdf4-python/."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from netCDF4 import date2num, num2date\n",
    "n2d = num2date(24, 'days since 2016-11-30')\n",
    "print(n2d, type(n2d))\n",
    "d2n = date2num(datetime.datetime.now(), 'days since 2016-11-30')\n",
    "print(d2n, type(d2n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This also works with lists and arrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(num2date([24, 24.5],\n",
    "               'days since 2016-11-30'))\n",
    "print()\n",
    "print(num2date(np.linspace(24, 25, 3),\n",
    "               'days since 2016-11-30'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "How could you use the `num2date` function for creating the time stamps in the NO2 data files?  (Remember that the first column contains the day-of-year-1993, i.e., the number of days which passed since the beginning of 1993, with 1.0 being 1993-01-01T00:00:00)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "no2 = pd.read_csv('../data/no2-athens/130623TS.VisNO2A',\n",
    "                comment='*',\n",
    "                delim_whitespace=True,\n",
    "                header=None # don't care about proper column names\n",
    "                )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using not-well-defined timedeltas\n",
    "Some timedelta units, e.g., seconds, days, minutes, are uniquely defined periods of time.  However, for longer periods, the common units are not uniquely defined any more.  For example a month can have different number of days, and years can have different number of days (gap years), and sometimes there are gap seconds and more esoteric things."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "datetime.datetime.now() + datetime.timedelta(months=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To deal with the easier of these (i.e., months and years), there is `dateutil.relativedelta`.  Documentation is available at https://dateutil.readthedocs.io/en/stable/relativedelta.html."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from dateutil.relativedelta import relativedelta\n",
    "datetime.datetime.now() + relativedelta(months=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also combine several units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "datetime.datetime.now() + relativedelta(months=1, weeks=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, you can set a certain point in time after a given period has passed (starting at the beginning of today)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# 10:00:00 after one month has passed from the beginning of today\n",
    "datetime.datetime.now() + relativedelta(months=1,\n",
    "                                        hour=10,\n",
    "                                        minute=0, second=0,\n",
    "                                        microsecond=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Beware of singular / plural language\n",
    "datetime.datetime.now() + relativedelta(months=1,\n",
    "                                        hours=10,\n",
    "                                        minute=0, second=0,\n",
    "                                        microsecond=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Create a datetime object for 12:00 of today one month ago.  What different ways to do this can you think about?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time periods instead of points in time\n",
    "Sometimes, it can be useful to work with *time periods* instead of *points in time*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In principle, `datetime.date` represents a *day*, so it is a time period.  However, the `datetime.date` object is really only a date, it does not \"know\" the notion of *duration*.\n",
    "\n",
    "Similar is true for `np.datetime64`, which is a point in time with a given resolution, without notion of *start* and *end*.\n",
    "\n",
    "In Pandas, there is the `pd.Period` object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dec = pd.Period('2016-12', 'M')\n",
    "print(dec, type(dec))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dec.start_time, dec.end_time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dec.start_time <= datetime.datetime(2016, 12, 24) <= dec.end_time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Parsing strings for date information\n",
    "In addition to the above mentioned methods to create time stamps from strings (`datetime.datetime.strptime()` and the direct usage of strings in `np.datetime64` and `pd.Timestamp` initialization), there is the module `dateutil.parser` which can sometimes be useful.  Documentation is available at https://dateutil.readthedocs.io/en/stable/parser.html."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from dateutil.parser import parse as parse_date\n",
    "pd1 = parse_date('Thu Sep 25 10:36:28 2003')\n",
    "pd2 = parse_date('2003-09-25 10:36:28')\n",
    "pd3 = parse_date('25.09.2003 10:36:28')\n",
    "pd4 = parse_date('09/25/2003 10:36:28')\n",
    "pd5 = parse_date('25 Sep 2003 10:36:28')\n",
    "assert pd1 == pd2 == pd3 == pd4 == pd5\n",
    "print(pd1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# !!! CAUTION !!!\n",
    "pd1 = parse_date('10 Sep 2003 10:36:28')\n",
    "pd2 = parse_date('09/10/2003 10:36:28')\n",
    "pd3 = parse_date('10.09.2003 10:36:28')\n",
    "pd4 = parse_date('13.09.2003 10:36:28')\n",
    "print(pd3)\n",
    "print(pd4)\n",
    "assert pd1 == pd2 == pd3"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
