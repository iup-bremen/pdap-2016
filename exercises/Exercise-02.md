Title: Exercise 02
Date: 01 December 2016


This is the second homework exercise for the course *Practical Data Analysis
with Python* by Andreas Hilboll, given at the University of Bremen in the winter
term 2016/17.


# Deadline

This optional exercise is due Wednesday, 07 December 2016, 10:00 CET.


# Overview

In this exercise you will again analyze measurements of atmospheric nitrogen
dioxide (NO2) from a measurement station located in Athens, Greece.


# Objective

This exercise will let you experience how using the Pandas library makes data
analysis more intuitive.


# Tasks

1. Create a function to read one file of NO2 measurements into a Pandas
   `DataFrame`.  The function should take one argument (the file name), and
   return the `DataFrame`.  The `DataFrame` should have the measurement's time
   stamp as *Index*, and should contain the NO2 slant column, the solar zenith
   angle (the angle between the vertical and the sun's position: at SZA=0°, the
   sun is in zenith, and at SZA=90°, the sun is just at the horizon), and the
   elevation angle.
2. Use this function to create one `DataFrame` of all measurements in all
   azimuth directions, and store this `DataFrame` in a *HDF5* file.  The azimuth
   direction should be contained in the `DataFrame` as an additional column.
3. Calculate the average diurnal (i.e., daily) cycle for the 2° elevation for
   each of the azimuth directions, and save this data to the same *HDF5* file as
   before.
4. Plot the average diurnal cycle for the 2° elevation.  Try two different
   approaches (all azimuth directions in one plot vs. one plot for each azimuth
   direction).
5. Create a `DataFrame` containing minimum, maximum, mean, median, and standard
   deviation over all measurements, with the azimuth direction as columns and
   the strings `'minimum'`, ``'maximum'``, ... as index, and save this data to
   the same *HDF5* file as before.
5. Create a bar plot showing this information.  Again, try two different
   approaches (all azimuth directions in one plot vs. one plot for each azimuth
   direction).


# Hints

- All data files are located in the folder `data/no2-athens/`.
- The filename of the NO2 data files (`*.VisNO2A`) contains two pieces of
  information, the date (in the form `YYMMDD`) and one of five viewing azimuth
  directions (`SS`, `TS`, `US`, `VS`, `WS`).  For example, the file
  `130624VS.VisNO2A` contains measurements from 24 Jun 2013 for the azimuth
  direction `VS`.
- The NO2 slant column density is contained in the column *Schräge Säule NO2*
- The column *Day of Year 1993* contains the days which passed since
  1992-12-31T00:00:00 UTC.  This means that for example 23 Jun 2013 has values
  between 7479.0 and 7480.0.
- For reading the files with `pandas.read_csv`, use `skipinitialspace=True` and
  `sep=' '`.
- In order to create a timestamp from the day-of-year and time columns, first
  use the day-of-year column's integer value as `days` argument and the time
  column's value as `hours` argument to `datetime.timedelta`, and use this
  together with the epoch offset of 1992-12-31.
- The column *Line of Sight* contains the elevation angle in degrees.
- For calculating the average diurnal cycle, use the `resample` method to
  calculate hourly values (mean or median).
  
  
## General comments

- Use functions to make your code more understandable.
- Document your code, so that you still remember two weeks from now why you
  wrote the code you write.
- Use
  [defensive programming](https://swcarpentry.github.io/python-novice-inflammation/08-defensive/),
  to find mistakes early.


# Logistics

You can complete this exercise in teams of two or three students. Please let
`LASTNAMES` be a `+` separated list, for example
`LASTNAME1+LASTNAME2+LASTNAME3`.

1.  If you haven't already done so, fork the course repository
    https://gitlab.com/iup-bremen/pdap-2016 and check out a local working copy
    to your computer.  Don't forget to give at least *Reporter* permissions to
    my Gitlab user *andreas-h*, to add your name, study program, and computer
    details to the file `PARTICIPANTS.md`, and to commit the changes to the
    *master* branch.
2.  Create a branch `ex02-LASTNAMES`.
6.  Copy this file (`exercises/Exercise-02.md`) to file
    `exercises/ex02/LASTNAMES.md`, and fill out the *Participants* section (see
    below).
7.  Create a notebook `exercises/ex02/LASTNAMES.ipynb` and solve the tasks
    outlined above; commit that file to the `ex02-LASTNAMES` branch.
8.  If necessary, give any additional information I need in order to understand
    your code in the section *Solution* below.
9.  Feel free to give feedback to this exercise in the section *Feedback*
    below. Please indicate the total time you needed to complete this exercise.
10. Don't forget to commit and push your changes to Gitlab.


# Participants

Names of all members of this study group:

1. 
2. 
3. 


# Solution


# Feedback
