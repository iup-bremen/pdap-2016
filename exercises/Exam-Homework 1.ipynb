{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PDAP-2016: Exam Homework Exercise 1\n",
    "\n",
    "## Winter term 2016/17\n",
    "## University of Bremen / Dr. Andreas Hilboll"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the first graded homework assignment for the course *Practical Data Analysis with Python*.  There will be a second graded homework assignment in late January; your total course grade will consist of the average of the two homework assignments.\n",
    "\n",
    "## Rules and regulations\n",
    "\n",
    "### When to submit\n",
    "This homework must be submitted by Thursday, 22 December 2016, 23:59:59 CET\n",
    "\n",
    "### How to submit\n",
    "You can submit this homework assignment by sending the IPython notebook (`.ipynb` file) to [hilboll@uni-bremen.de](mailto:hilboll@uni-bremen.de).  You can also (additionally) commit your homework to your Gitlab repository.  Don't forget you need to give at least *reporter* access to my Gitlab user `andreas-h`.\n",
    "\n",
    "**Note:** It is *your* responsibility to make sure that your homework submission reaches me in time.  If in doubt, submit early and ask me if I received your file.\n",
    "\n",
    "### Technical requirements\n",
    "Your solution has to be written to this IPython Notebook.  Please rename the notebook so that your last name(s) is/are included in the filename.  Your solution has to consist of exactly one (i.e., this) file.\n",
    "\n",
    "All paths have to be defined so that the code will run on my computer when I place the `.ipynb` file into the `exercises` subfolder of the course repository.\n",
    "\n",
    "### Study groups\n",
    "You are allowed to complete this homework assignment either alone or in groups of up to three students.  In case you do not do your homework alone, please clearly state who has contributed how much to which part of the homework.\n",
    "\n",
    "### Discussion\n",
    "In January, there will be a separate ~10-15 minute oral discussion for each study group, in which all students are expected be able to demonstrate that they understand the code they submitted.\n",
    "\n",
    "### Using internet resources\n",
    "You are allowed to use book and/or internet resources to complete this homework assignment.  However, you are expected to a) clearly state any reference you have used to complete the assigment (e.g., by giving the URL to a website in a code comment), and b) to be able to explain the code you are writing.\n",
    "\n",
    "### Evaluation criteria\n",
    "You will be graded on all *tasks* laid out below.  There are many possibilities to earn extra credit, clearly indicated in the task description.\n",
    "\n",
    "The following criteria will all contribute to your grade:\n",
    "- *Completeness:* Your code should address all aspects of the assignment\n",
    "- *Correctness:* Your code should yield the correct results\n",
    "- *Readability:* Your code should be clearly readable; ideally, one should be able to *quickly* understand its meaning without any prior knowledge\n",
    "- *Understandability:* The plots you create should be easily understandable\n",
    "- *Understanding:* In the oral discussion in January, you should be able to explain your solution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Background\n",
    "\n",
    "NO2 is a trace gas which is produced mainly from the burning of fossil fuels;\n",
    "other (natural) sources include biomass burning (forest fires, agricultural\n",
    "fires), lightning, and microbial emissions from soils.\n",
    "\n",
    "MAX-DOAS stands for *Multi-AXis Differential Optical Absorption Spectroscopy*.\n",
    "The instrument consists of a telescope and a spectrometer, which measure the\n",
    "intensity of scattered sunlight in different elevation and azimuth directions.\n",
    "DOAS is an application of the Beer-Lambert law, in which the integrated trace\n",
    "gas concentration along the average light path (from the sun to the instrument),\n",
    "called *slant column* or *slant column density*, is derived from the trace gas'\n",
    "absorption cross section (measured in the laboratory) and the attenuation of the\n",
    "scattered sunlight in the atmosphere.  The slant column density is in units of\n",
    "*molecules per ground area*.  As it depends strongly on the length of the light\n",
    "path, it is larger close to sunrise and sunset, when the sun is low, compared to\n",
    "midday, when the sun is high.\n",
    "\n",
    "The elevation angle is the angle between the vertical (pointing downwards) and\n",
    "the viewing elevation of the telescope, i.e., it is 90° for looking towards the\n",
    "horizon and 180° for looking towards the zenith.\n",
    "\n",
    "The azimuth angle is the geographical direction of the telescope line-of-sight.\n",
    "In these data files, it is defined to go from -180° to 180°, with -90° being\n",
    "East, 0° being South, and 90° being West.\n",
    "\n",
    "## Technical comments\n",
    "- The filename of the NO2 data files (`*.VisNO2A`) contains two pieces of\n",
    "  information, the date (in the form `YYMMDD`) and one of five viewing azimuth\n",
    "  directions (`SS`, `TS`, `US`, `VS`, `WS`).  For example, the file\n",
    "  `130624VS.VisNO2A` contains measurements from 24 Jun 2013 for the azimuth\n",
    "  direction `VS`.\n",
    "- The data files start with a description of the file contents. Each of the first comment lines, starting with `*`, contains information on the contents of one column.  E.g., the first column contains information on *Day of Year 1993* and the second column contains information on *Uhrzeit [UT]*.\n",
    "- The NO2 slant column density is contained in the column *Schräge Säule NO2*\n",
    "- The column *Day of Year 1993* contains the days which passed since\n",
    "  1992-12-31T00:00:00 UTC.  This means that for example 23 Jun 2013 has values\n",
    "  between 7479.0 and 7480.0.\n",
    "- The column *Line of Sight* contains the elevation angle in degrees."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 0: Participants\n",
    "\n",
    "Please fill in your personal details into the following table:\n",
    "\n",
    "| First name | Last name | Study program | Student ID |\n",
    "|------------|-----------|---------------|------------|\n",
    "| Jane       | Doe       | PEP           | 1234567    |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 1: NO2 data analysis for the Athens measurements\n",
    "\n",
    "### Data download\n",
    "Download the NO2 data files from here: https://seafile.zfn.uni-bremen.de/f/7ebe872f38/?raw=1 (download size ~85M; uncompressed size ~260M) and unzip it to a new folder `data/no2-athens/2013` in your course repository. **Note:** Do not commit this directory to version control!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.1\n",
    "Create a data frame consisting of all NO2 measurements for the year 2013.  The data frame should have the measurement timestamp as *Index*, and should have the four columns *NO2 slant column*, *solar zenith angle*, *azimuth direction*, *elevation angle*.  Save this data frame to a HDF file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.2\n",
    "Choose one azimuth direction.  For this azimuth direction, calculate the average NO2 for each elevation and month."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.3\n",
    "For the same azimuth direction as in *Task 1.2*, calculate the annual mean NO2 for each elevation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.4\n",
    "There is more than one way of calculating the annual mean:\n",
    "\n",
    "- Directly calculate the mean for each elevation from the data frame containing all measurements\n",
    "- Calculate the annual mean as the average of the monthly mean values\n",
    "\n",
    "#### Task 1.4.1\n",
    "Explain under which circumstances the two methods can yield different results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task 1.4.2\n",
    "Implement the variant you did not choose in *Task 1.3* and visually compare the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.5\n",
    "Write a function, which takes as inputs\n",
    "\n",
    "1. the data frame you created in *Task 1.1*\n",
    "2. the azimuth direction\n",
    "3. the elevation angle\n",
    "\n",
    "and returns a data frame of average diurnal cycle for the specified azimuth and elevation, for each month, i.e., the data frame should have the month (`Jan`, `Feb`, ...) as columns and the time as index.\n",
    "\n",
    "*Hint:* First write a helper function which takes the same inputs as before plus the month as fourth input, and returns as a series the average diurnal cycle for this month. Then, use this helper function inside a loop to solve this task."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.6\n",
    "Choose any one azimuth direction.  For this azimuth direction, plot the average diurnal cycle for each month and elevation angle.  Do this by creating one subplot for each elevation angle, with all months for the same elevation angle within the same plot.  Use different colors for each month."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.7\n",
    "Choose any one elevation angle and any pair of two azimuth directions.  For this subset of data, create a scatter plot of all daily mean NO2 values (one azimuth direction on *x*, the other azimuth direction on *y*)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task 1.7.1 (extra credit)\n",
    "As in *Task 1.7*, but give each scatter point a color indicating the month, and include a legend for the different colors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task 1.7.2 (extra credit)\n",
    "Calculate the regression line for this data set, plot the line into the Figure, and write the slope as an annotation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.8 (extra credit)\n",
    "Use the *xarray* module to create a four-dimensional array holding the average diurnal cycle for all months, elevation angles, and azimuth directions, and save this array to a netCDF file.\n",
    "\n",
    "*Hint 1:* You will probably need to install *xarray* using the command `conda install xarray` (on the command line).\n",
    "\n",
    "*Hint 2:* The *xarray* documentation is available at http://xarray.pydata.org/en/v0.8.2/.\n",
    "\n",
    "*Hint 3:* You will want to create a 4D `xarray.DataArray`, with the dimensions *month*, *time*, *azimuth*, and *elevation*, and iteratively fill this array in loops over azimuth and elevation, using the functions from above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 2: Analysis of the Mauna Loa CO2 time series"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 2.1\n",
    "Create a data frame of the Mauna Loa CO2 measurements, available at ftp://aftp.cmdl.noaa.gov/products/trends/co2/co2_mm_mlo.txt, and plot the CO2 time series."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 2.2\n",
    "Calculate annual averages from the CO2 data and plot these."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 2.3\n",
    "Create a data frame from the monthly average temperature in Bremen (source: https://seafile.zfn.uni-bremen.de/f/c64885e5c0/?raw=1).  The first and last day of the measurement month are encoded in the columns `MESS_DATUM_BEGINN` and `MESS_DATUM_ENDE`, respectively, and the aveage temperature is stored in the column `LUFTTEMPERATUR`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 2.4\n",
    "Create a scatter plot of CO2 in Hawaii vs. temperature in Bremen, using all available monthly data.\n",
    "\n",
    "**Extra credit:** Give each scatter point a color depending on the year, in a meaningful way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
